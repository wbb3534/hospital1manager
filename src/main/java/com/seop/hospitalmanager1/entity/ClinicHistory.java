package com.seop.hospitalmanager1.entity;

import com.seop.hospitalmanager1.enums.MedicalItem;
import com.seop.hospitalmanager1.interfaces.CommonModelBuilder;
import com.seop.hospitalmanager1.model.ClinicHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ClinicHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long historyId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospitalCustomerId", nullable = false)
    private HospitalCustomer hospitalCustomer;
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private MedicalItem medicalItem;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private Boolean isCost;
    @Column(nullable = false)
    private LocalDate dateCure;
    @Column(nullable = false)
    private LocalTime timeCure;
    @Column(nullable = false)
    private Boolean isCalculate;

    public void putCompletePay() {
        this.isCalculate = true;
    }

    private ClinicHistory(ClinicHistoryBuilder builder) {
        this.hospitalCustomer = builder.customer;
        this.medicalItem = builder.medicalItem;
        this.price = builder.price;
        this.isCost = builder.isCost;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculate = builder.isCalculate;

    }

    public static class ClinicHistoryBuilder implements CommonModelBuilder<ClinicHistory> {
        private final HospitalCustomer customer;
        private final MedicalItem medicalItem;
        private final Double price;
        private final Boolean isCost;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final Boolean isCalculate;

        public ClinicHistoryBuilder(HospitalCustomer customer, ClinicHistoryRequest request) {
            this.customer = customer;
            this.medicalItem = request.getMedicalItem();
            this.price = request.getIsCost() ? request.getMedicalItem().getSalaryPrice() : request.getMedicalItem().getNonSalaryPrice();
            this.isCost = request.getIsCost();
            this.dateCure = LocalDate.now();
            this.timeCure = LocalTime.now();
            this.isCalculate = false;
        }

        @Override
        public ClinicHistory build() {
            return new ClinicHistory(this);
        }
    }
}
