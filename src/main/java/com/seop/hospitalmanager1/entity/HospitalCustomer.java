package com.seop.hospitalmanager1.entity;

import com.seop.hospitalmanager1.interfaces.CommonModelBuilder;
import com.seop.hospitalmanager1.model.HospitalCustomerRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HospitalCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 13)
    private String customerPhone;
    @Column(nullable = false, length = 14)
    private String residentNum;
    @Column(nullable = false, length = 100)
    private String address;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String memo;
    @Column(nullable = false)
    private LocalDate visitFirst;

    private HospitalCustomer(HospitalCustomerBuilder builder) {
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.residentNum = builder.residentNum;
        this.address = builder.address;
        this.memo = builder.memo;
        this.visitFirst = builder.visitFirst;
    }

    public static class HospitalCustomerBuilder implements CommonModelBuilder<HospitalCustomer> {
        private final String customerName;
        private final String customerPhone;
        private final String residentNum;
        private final String address;
        private final String memo;
        private final LocalDate visitFirst;

        public HospitalCustomerBuilder(HospitalCustomerRequest request) {
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.residentNum = request.getResidentNum();
            this.address = request.getAddress();
            this.memo = request.getMemo();
            this.visitFirst = LocalDate.now();
        }

        @Override
        public HospitalCustomer build() {
            return new HospitalCustomer(this);
        }
    }
}
