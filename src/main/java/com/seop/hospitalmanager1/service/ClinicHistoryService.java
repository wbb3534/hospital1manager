package com.seop.hospitalmanager1.service;

import com.seop.hospitalmanager1.entity.ClinicHistory;
import com.seop.hospitalmanager1.entity.HospitalCustomer;
import com.seop.hospitalmanager1.model.ClinicHistoryItem;
import com.seop.hospitalmanager1.model.ClinicHistoryRequest;
import com.seop.hospitalmanager1.model.ClinicHistoryResponse;
import com.seop.hospitalmanager1.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClinicHistoryService {
    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistory(HospitalCustomer hospitalCustomer, ClinicHistoryRequest request) {
        ClinicHistory addData = new ClinicHistory.ClinicHistoryBuilder(hospitalCustomer, request).build();

        clinicHistoryRepository.save(addData);
    }

    public List<ClinicHistoryItem> getHistoryByDate(LocalDate date) {
        List<ClinicHistory> originList = clinicHistoryRepository.findByDateCureOrderByHistoryIdDesc(date);

        List<ClinicHistoryItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            ClinicHistoryItem addItem = new ClinicHistoryItem.ClinicHistoryItemBuilder(item).build();

            result.add(addItem);
        }
        return result;
    }

    public ClinicHistoryResponse getHistory(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();

        return new ClinicHistoryResponse.ClinicHistoryResponseBuilder(originData).build();
    }

    public void putCompletePay(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();
        originData.putCompletePay();

        clinicHistoryRepository.save(originData);
    }
}
