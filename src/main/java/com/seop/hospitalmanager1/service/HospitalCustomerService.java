package com.seop.hospitalmanager1.service;

import com.seop.hospitalmanager1.entity.HospitalCustomer;
import com.seop.hospitalmanager1.model.HospitalCustomerRequest;
import com.seop.hospitalmanager1.repository.HospitalCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HospitalCustomerService {
    private final HospitalCustomerRepository hospitalCustomerRepository;

    public void setCustomer(HospitalCustomerRequest request) {
        HospitalCustomer addData = new HospitalCustomer.HospitalCustomerBuilder(request).build();
        hospitalCustomerRepository.save(addData);
    }
    public HospitalCustomer getCustomerId(long id) {
        return hospitalCustomerRepository.findById(id).orElseThrow();
    }
}
