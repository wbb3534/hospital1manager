package com.seop.hospitalmanager1.repository;

import com.seop.hospitalmanager1.entity.ClinicHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ClinicHistoryRepository extends JpaRepository<ClinicHistory, Long> {
    List<ClinicHistory> findByDateCureOrderByHistoryIdDesc(LocalDate localDate);
}
