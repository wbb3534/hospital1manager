package com.seop.hospitalmanager1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalManager1Application {

    public static void main(String[] args) {
        SpringApplication.run(HospitalManager1Application.class, args);
    }

}
