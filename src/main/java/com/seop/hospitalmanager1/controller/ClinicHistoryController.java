package com.seop.hospitalmanager1.controller;

import com.seop.hospitalmanager1.entity.HospitalCustomer;
import com.seop.hospitalmanager1.model.ClinicHistoryItem;
import com.seop.hospitalmanager1.model.ClinicHistoryRequest;
import com.seop.hospitalmanager1.model.ClinicHistoryResponse;
import com.seop.hospitalmanager1.repository.ClinicHistoryRepository;
import com.seop.hospitalmanager1.repository.HospitalCustomerRepository;
import com.seop.hospitalmanager1.service.ClinicHistoryService;
import com.seop.hospitalmanager1.service.HospitalCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/v1/clinic-history")
@RequiredArgsConstructor
public class ClinicHistoryController {
    private final HospitalCustomerService hospitalCustomerService;
    private final ClinicHistoryService clinicHistoryService;

    @PostMapping("/new/customer/{customerId}")
    public String setHistory(@PathVariable long customerId, @RequestBody @Valid ClinicHistoryRequest request) {
        HospitalCustomer customer = hospitalCustomerService.getCustomerId(customerId);
        clinicHistoryService.setHistory(customer, request);
        return "ok";
    }

    @GetMapping("/info/date")
    public List<ClinicHistoryItem> getHistoryByDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return clinicHistoryService.getHistoryByDate(searchDate);
    }
    @GetMapping("/info/history/{historyId}")
    public ClinicHistoryResponse getHistory(@PathVariable long historyId) {
        return clinicHistoryService.getHistory(historyId);
    }
    @PutMapping("/pay-complete/history/{historyId}")
    public String putComplete(@PathVariable long historyId) {
        clinicHistoryService.putCompletePay(historyId);

        return "Ok";
    }
}
