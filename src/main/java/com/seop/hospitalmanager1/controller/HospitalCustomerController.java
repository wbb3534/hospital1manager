package com.seop.hospitalmanager1.controller;

import com.seop.hospitalmanager1.model.HospitalCustomerRequest;
import com.seop.hospitalmanager1.service.HospitalCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/hospital-customer")
public class HospitalCustomerController {
    private final HospitalCustomerService hospitalCustomerService;

    @PostMapping("/new")
    public String setHospitalCustomer(@RequestBody @Valid HospitalCustomerRequest request) {
        hospitalCustomerService.setCustomer(request);

        return "Ok";
    }
}
