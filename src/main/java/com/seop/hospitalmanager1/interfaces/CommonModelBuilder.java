package com.seop.hospitalmanager1.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
