package com.seop.hospitalmanager1.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HospitalCustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 13, max = 13)
    private String customerPhone;
    @NotNull
    @Length(min = 10, max = 15)
    private String residentNum;
    @NotNull
    @Length(min = 2, max = 100)
    private String address;
    @NotNull
    private String memo;
}
