package com.seop.hospitalmanager1.model;

import com.seop.hospitalmanager1.entity.ClinicHistory;
import com.seop.hospitalmanager1.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ClinicHistoryResponse {
    private Long customerId;
    private Long historyId;
    private String customerName;
    private String customerPhone;
    private String customerResidentNum;
    private String customerAddress;
    private String memo;
    private LocalDate visitFirst;
    private String medicalItem;
    private Double price;
    private Double nonSalary;
    private String isSalary;
    private LocalDate cureDate;
    private LocalTime cureTime;
    private String isCalculate;

    private ClinicHistoryResponse(ClinicHistoryResponseBuilder builder) {
        this.customerId = builder.customerId;
        this.historyId = builder.historyId;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.customerAddress = builder.customerAddress;
        this.customerResidentNum = builder.customerResidentNum;
        this.memo = builder.memo;
        this.visitFirst = builder.visitFirst;
        this.medicalItem = builder.medicalItem;
        this.price = builder.price;
        this.nonSalary = builder.nonSalary;
        this.isSalary = builder.isSalary;
        this.cureDate = builder.cureDate;
        this.cureTime = builder.cureTime;
        this.isCalculate = builder.isCalculate;

    }
    public static class ClinicHistoryResponseBuilder implements CommonModelBuilder<ClinicHistoryResponse> {
        private final Long customerId;
        private final Long historyId;
        private final String customerName;
        private final String customerPhone;
        private final String customerResidentNum;
        private final String customerAddress;
        private final String memo;
        private final LocalDate visitFirst;
        private final String medicalItem;
        private final Double price;
        private final Double nonSalary;
        private final String isSalary;
        private final LocalDate cureDate;
        private final LocalTime cureTime;
        private final String isCalculate;

        public ClinicHistoryResponseBuilder(ClinicHistory clinicHistory) {
            this.customerId = clinicHistory.getHospitalCustomer().getId();
            this.historyId = clinicHistory.getHistoryId();
            this.customerName = clinicHistory.getHospitalCustomer().getCustomerName();
            this.customerPhone = clinicHistory.getHospitalCustomer().getCustomerPhone();
            this.customerAddress = clinicHistory.getHospitalCustomer().getAddress();
            this.customerResidentNum = clinicHistory.getHospitalCustomer().getResidentNum();
            this.memo = clinicHistory.getHospitalCustomer().getMemo();
            this.visitFirst = clinicHistory.getHospitalCustomer().getVisitFirst();
            this.medicalItem = clinicHistory.getMedicalItem().getName();
            this.price = clinicHistory.getPrice();
            this.nonSalary = clinicHistory.getMedicalItem().getNonSalaryPrice();
            this.isSalary = clinicHistory.getIsCost() ? "Y" : "N";
            this.cureDate = clinicHistory.getDateCure();
            this.cureTime = clinicHistory.getTimeCure();
            this.isCalculate = clinicHistory.getIsCalculate() ? "T" : "N";
        }
        @Override
        public ClinicHistoryResponse build() {
            return new ClinicHistoryResponse(this);
        }
    }
}
