package com.seop.hospitalmanager1.model;

import com.seop.hospitalmanager1.enums.MedicalItem;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class ClinicHistoryRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;
    @NotNull
    private Boolean isCost;
}
