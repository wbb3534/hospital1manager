package com.seop.hospitalmanager1.model;

import com.seop.hospitalmanager1.entity.ClinicHistory;
import com.seop.hospitalmanager1.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ClinicHistoryItem {
    private Long customerId;
    private Long historyId;
    private String customerName;
    private String customerPhone;
    private String customerResidentNum;
    private String medicalItem;
    private Double price;
    private String isSalary;
    private LocalDate cureDate;
    private LocalTime cureTime;
    private String isCalculate;

    private ClinicHistoryItem(ClinicHistoryItemBuilder builder) {
        this.customerId = builder.customerId;
        this.historyId = builder.historyId;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.customerResidentNum = builder.customerResidentNum;
        this.medicalItem = builder.medicalItem;
        this.price = builder.price;
        this.isSalary = builder.isSalary;
        this.cureDate = builder.cureDate;
        this.cureTime = builder.cureTime;
        this.isCalculate = builder.isCalculate;
    }
    public static class ClinicHistoryItemBuilder implements CommonModelBuilder<ClinicHistoryItem> {
        private final Long customerId;
        private final Long historyId;
        private final String customerName;
        private final String customerPhone;
        private final String customerResidentNum;
        private final String medicalItem;
        private final Double price;
        private final String isSalary;
        private final LocalDate cureDate;
        private final LocalTime cureTime;
        private final String isCalculate;

        public ClinicHistoryItemBuilder(ClinicHistory clinicHistory) {
            this.customerId = clinicHistory.getHospitalCustomer().getId();
            this.historyId = clinicHistory.getHistoryId();
            this.customerName = clinicHistory.getHospitalCustomer().getCustomerName();
            this.customerPhone = clinicHistory.getHospitalCustomer().getCustomerPhone();
            this.customerResidentNum = clinicHistory.getHospitalCustomer().getResidentNum();
            this.medicalItem = clinicHistory.getMedicalItem().getName();
            this.price = clinicHistory.getPrice();
            this.isSalary = clinicHistory.getIsCost() ? "Y" : "N";
            this.cureDate = clinicHistory.getDateCure();
            this.cureTime = clinicHistory.getTimeCure();
            this.isCalculate = clinicHistory.getIsCalculate() ? "Y" : "N";
        }
        @Override
        public ClinicHistoryItem build() {
            return new ClinicHistoryItem(this);
        }
    }
}
