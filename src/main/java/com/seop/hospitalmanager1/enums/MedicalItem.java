package com.seop.hospitalmanager1.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MedicalItem {
    CHUNA("추나치료", 70000, 70000),
    ACUPUNCTURE("약침치료", 10000,7000),
    PHYSICAL("물리치료", 20000, 10000)
    ;
    private final String name;
    private final double salaryPrice;
    private final double nonSalaryPrice;
}
